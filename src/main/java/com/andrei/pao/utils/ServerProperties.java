package com.andrei.pao.utils;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class ServerProperties implements LogProvider {

    private static ServerProperties instance;
    private static Properties properties = new Properties();

    private ServerProperties(){}

    public static ServerProperties getInstance(){
        if (!Optional.ofNullable(instance).isPresent()){
            instance = new ServerProperties();
        }

        return instance;
    }

    public void loadProperties() {
        try {
            properties.load(ServerProperties.class.getClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            logger().error("Cannot load app properties: " + e.getMessage());
        }
    }

    public static String getProperty(String reqProp) {
        return properties.getProperty(reqProp);
    }

    public static String getProperty(String reqProp, String defaultValue) {
        return properties.getProperty(reqProp, defaultValue);
    }
}
