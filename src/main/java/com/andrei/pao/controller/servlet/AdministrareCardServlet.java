package com.andrei.pao.controller.servlet;

import com.andrei.pao.exceptions.InvalidInputException;
import com.andrei.pao.db.dao.CardCRUD;
import com.andrei.pao.db.dbo.Card;
import com.andrei.pao.db.dbo.EntityBuilder;
import com.andrei.pao.model.CardOpType;
import com.andrei.pao.model.CardType;
import com.andrei.pao.utils.LogProvider;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@WebServlet(urlPatterns = "/adminCard")
public class AdministrareCardServlet extends HttpServlet implements LogProvider {

    private final CardCRUD cardCRUD = new CardCRUD();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletOutputStream os = resp.getOutputStream();

        CardType tipCard;
        String linie;
        int durata;
        double sumaDeAdaugat;

        try {

            try {
                tipCard = CardType.valueOf(req.getParameter("tipCard"));
                linie = req.getParameter("linie");
                durata = Integer.valueOf(req.getParameter("durata"));
                sumaDeAdaugat = Double.valueOf(req.getParameter("suma").trim());
            } catch (Exception e) {
                throw new InvalidInputException("Date incorecte. Reintroduceti");
            }

            Optional<Card> cardOptional = Optional.of(
                    cardCRUD.operateCard(
                            EntityBuilder.buildCard(req.getParameter("codCard").trim()),
                            CardOpType.GET_CARD
                    )
            );

            if (cardOptional.isPresent()) {
                Card cardData = cardOptional.get();

                CardOpType opType;
                switch (tipCard) {
                    case ABONAMENT:
                        cardData.setTipCard("ABONAMENT");
                        cardData.setTipLinie(linie);
                        cardData.setDataExpirareAbonament(LocalDateTime.now().plusDays(durata));
                        opType = CardOpType.ABONAMENT;
                        break;
                    case PORTOFEL:
                        if (cardData.getSumaDisponibila() + sumaDeAdaugat > 50) {
                            throw new InvalidInputException("Suma de adaugat mai mare decat asteptat. Suma curenta pe card: " + cardData.getSumaDisponibila() + " Reintroduceti.");
                        } else {
                            cardData.setSumaDisponibila(cardData.getSumaDisponibila() + sumaDeAdaugat);
                        }
                        opType = CardOpType.PORTOFEL;
                        break;
                    default:
                        opType = CardOpType.INVALID;
                        break;
                }

                cardData = cardCRUD.operateCard(cardData, opType);

                StringBuilder responseBuilder = new StringBuilder("Administrare Card! Client: ")
                        .append(cardData.getNume())
                        .append(" ")
                        .append(cardData.getPrenume())
                        .append(" Cod Card: ")
                        .append(cardData.getCodCard());

                if (Objects.equals(tipCard, CardType.ABONAMENT)) {
                    responseBuilder.append(" Card Incarcat cu abonament ");
                    if (durata == 1) {
                        responseBuilder.append(" de o zi pe ");
                    } else {
                        responseBuilder.append(" de o luna pe");
                    }

                    if (linie.equals("ALL")) {
                        responseBuilder.append(" toate liniile.");
                    } else {
                        responseBuilder.append(" linia ").append(cardData.getTipLinie()).append(".");
                    }

                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-dd-mm HH:mm");
                    responseBuilder.append(" Abonamentul expira pe data de ").append(cardData.getDataExpirareAbonament().format(formatter)).append(".");
                } else {
                    responseBuilder.append(" Card incarcat cu suma de ").append(sumaDeAdaugat).append(" in portofelul electronic.");
                    responseBuilder.append(" Suma totala curenta: ").append(cardData.getSumaDisponibila());
                }

                os.print(responseBuilder.toString());
            }
        } catch (SQLException sqle) {
            logger().error("Nu s-a putut stabili conexiunea la baza de date. Eroare: {}", sqle.getMessage());
            os.print("Nu s-a putut stabili conexiunea la baza de date. Eroare: " + sqle.getMessage());
            sqle.printStackTrace();
        } catch (InvalidInputException iie) {
            os.print("Administrare Card! Eroare: " + iie.getMessage());
        } finally {
            this.getServletContext().getContext("/").getRequestDispatcher("/index.jsp");
            os.close();
        }
    }

}
