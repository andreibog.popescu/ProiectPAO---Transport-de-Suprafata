package com.andrei.pao.db.dao;

import com.andrei.pao.exceptions.InvalidInputException;
import com.andrei.pao.db.DatabaseConnectionFactory;
import com.andrei.pao.db.dbo.Validare;
import com.andrei.pao.model.ValidareOpType;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class ValidatCRUD {

    private final String INSERT_VALIDARE_QUERY = "INSERT INTO tvalidare" +
            "(cod_card, data_validare, cod_masina, linie)" +
            " VALUES " +
            "(?,?,?,?)" +
            " RETURNING *;";

    private final String COD_MASINA_EXISTS_QUERY = "SELECT COUNT(*) " +
            "FROM tvalidare " +
            "WHERE cod_masina = ? " +
            "AND linie <> ?;";

    private final String GET_VALIDARI_SIMILARE_QUERY = "SELECT * " +
            "FROM tvalidare " +
            "WHERE cod_card = ? " +
            "AND linie = ? " +
            "AND cod_masina = ?;";

    public Validare operateValidare(Validare validare, ValidareOpType operationType) throws SQLException {
        try (
                Connection connection = DatabaseConnectionFactory.getNewConnection();
                PreparedStatement preparedStatement = defineStatement(connection, operationType, validare)
        ) {
            Optional<PreparedStatement> optionalPreparedStatement = Optional.ofNullable(preparedStatement);
            if (optionalPreparedStatement.isPresent()) {
                optionalPreparedStatement.get().execute();

                validare = updateValidareFromResultSet(validare, optionalPreparedStatement.get().getResultSet());
            }

            return validare;
        }
    }

    private Validare updateValidareFromResultSet(Validare validare, ResultSet resultSet) throws SQLException {
        if (resultSet.next()){
            validare.setCodCard(String.valueOf(resultSet.getLong("cod_card")));
            validare.setCodMasina(String.valueOf(resultSet.getString("cod_masina")));
            validare.setLinie(resultSet.getString("linie"));

            Timestamp dataValidareTS = resultSet.getTimestamp("data_validare");
            validare.setDataValidare(LocalDateTime.ofInstant(Instant.ofEpochMilli(dataValidareTS.getTime()), ZoneId.systemDefault()));

            resultSet.close();
        }
        return validare;
    }

    public boolean validateIfDataIsCorrect(Validare validare, boolean validareCuPortofel) throws InvalidInputException, SQLException {
        try (
                Connection connection = DatabaseConnectionFactory.getNewConnection();
                PreparedStatement codMasinaExistsStatement = defineStatement(connection, ValidareOpType.COD_MASINA_EXISTS, validare);
                PreparedStatement validareExistentaStatement = defineStatement(connection, ValidareOpType.GET_VALIDARI_SIMILARE, validare)
        ) {
            Optional<PreparedStatement> optionalPreparedStatement = Optional.ofNullable(codMasinaExistsStatement);
            if (optionalPreparedStatement.isPresent()) {
                optionalPreparedStatement.get().execute();
                ResultSet rs = optionalPreparedStatement.get().getResultSet();

                if (rs.next()) {
                    if (rs.getInt(1) != 0) {
                        throw new InvalidInputException("Codul masina " + validare.getCodMasina() + " exista deja pentru o alta linie.");
                    }
                }

                rs.close();
            }

            optionalPreparedStatement = Optional.ofNullable(validareExistentaStatement);
            if (optionalPreparedStatement.isPresent()) {
                optionalPreparedStatement.get().execute();
                ResultSet rs = optionalPreparedStatement.get().getResultSet();

                while (rs.next()) {
                    Timestamp dataValidareTS = rs.getTimestamp("data_validare");
                    LocalDateTime dataValidare = LocalDateTime.ofInstant(Instant.ofEpochMilli(dataValidareTS.getTime()), ZoneId.systemDefault());

                    if (validare.getDataValidare().isBefore(dataValidare)){
                        throw new InvalidInputException("O validare nu se poate face inainte de data oricarei alte validari pe aceeasi linie si cod masina.");
                    }

                    if (Math.abs(validare.getDataValidare().until(dataValidare, ChronoUnit.MINUTES)) < 60 && !validareCuPortofel){
                        throw new InvalidInputException("Card validat deja. " +
                                "(Validarea poate avea loc doar o singura data la interval de 60 de minute pentru carduri de tip Abonament; " +
                                "Ultima validare a avut loc acum " + Math.abs(validare.getDataValidare().until(dataValidare, ChronoUnit.MINUTES)) + " minute" +
                                ").");
                    }
                }

                rs.close();
            }

            return true;
        }
    }

    private PreparedStatement defineStatement(Connection connection, ValidareOpType operationType, Validare validare) throws SQLException {
        PreparedStatement preparedStatement;
        switch (operationType) {
            case ADD_VALIDARE:
                preparedStatement = connection.prepareStatement(INSERT_VALIDARE_QUERY);
                preparedStatement.setLong(1, Long.valueOf(validare.getCodCard()));
                preparedStatement.setTimestamp(2, new Timestamp(validare.getDataValidare().toInstant(ZoneId.systemDefault().getRules().getOffset(Instant.now())).toEpochMilli()));
                preparedStatement.setLong(3, Long.valueOf(validare.getCodMasina()));
                preparedStatement.setString(4, validare.getLinie());

                return preparedStatement;
            case COD_MASINA_EXISTS:
                preparedStatement = connection.prepareStatement(COD_MASINA_EXISTS_QUERY);
                preparedStatement.setLong(1, Long.valueOf(validare.getCodMasina()));
                preparedStatement.setString(2, validare.getLinie());

                return preparedStatement;
            case GET_VALIDARI_SIMILARE:
                preparedStatement = connection.prepareStatement(GET_VALIDARI_SIMILARE_QUERY);
                preparedStatement.setLong(1, Long.valueOf(validare.getCodCard()));
                preparedStatement.setString(2, validare.getLinie());
                preparedStatement.setLong(3, Long.valueOf(validare.getCodMasina()));

                return preparedStatement;
            default:
                return null;
        }
    }
}
